# README #

Shell script wich switches user development profiles

### How do I get set up? ###

I recommend you to backup your SSH keys before the first use.

Steps:

1 - Create a new txt file within project folder (or use the examples)
    - The file must contain 2 lines: the first is your email account
    - The second line is a link to your ssh key (without the .pub extension)
Example: 
    - #see Example file, just add the .txt extension, so script will list as a valid profile#

2 - You will need to give execution permissions, so run 'chmod +x profile_switch.sh' and then,
you can run it.

You can use it by sending the profile id as parameter './profile_switch.sh 0 will also work and
switch with profile 0.

### How it works ###

Script will read all the txt files and ask you to chose an option. 
Then, it reads the selected option text file, replace git global configs,
remove the current id_rsa files and copy with those listed in profiles.

You can also use it by typing the profile id

### IMPORTANT: Files will be erased ###

This is a WIP project, so use it carefully.
