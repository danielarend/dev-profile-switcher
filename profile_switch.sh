#!/bin/bash

echo "----------------------------------------------"
echo "-----------SSH/Git Profile Switcher-----------"
echo "----------------------------------------------"
echo ""
echo "Here goes your profiles list"
echo ""
echo ""
echo "TIP: if you already know your profile id"
echo "you can run directly ./profile_switch N"
echo "where N represents the profile ID "
echo ""
cd ~/dev-profile-switcher


yourfilenames=`ls *.txt`
n=0
arrProfiles=()

for eachfile in $yourfilenames
do
   arrProfiles+=($eachfile)
   echo "    - Profile [$n] : $eachfile"
   n=$((n+1))
done

echo ""
echo "type the number to switch into SSH/Git Profile"

if [ "$1" != "" ]; then
    input_profile=$1
else
    read input_profile
fi

echo "Choosen Profile: ${arrProfiles[$input_profile]}"
echo ""
n=1
while read line; do
# reading each line
    echo "    - $line"
    case $n in
    1) git config --global user.email "$line";;
    2) rm -f $HOME/.ssh/id_rsa.pub && rm -f $HOME/.ssh/id_rsa && cp $HOME$line.pub $HOME/.ssh/id_rsa.pub && cp $HOME$line $HOME/.ssh/id_rsa;;
    esac
    n=$((n+1))
done < ${arrProfiles[$input_profile]}

echo ""
echo "Finished"